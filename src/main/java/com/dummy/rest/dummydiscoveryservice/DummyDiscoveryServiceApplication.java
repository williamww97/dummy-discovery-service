package com.dummy.rest.dummydiscoveryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DummyDiscoveryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummyDiscoveryServiceApplication.class, args);
	}

}
